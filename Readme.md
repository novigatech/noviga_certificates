## Steps to be followed for Nucleus automation.

All the steps below should be followed from a client machine connected to the pi which is to be automated. You may not be able to login back to pi using keyboard and monitor after it reboots.

### Initial Steps

#### *Step#1:*
```
sudo apt-get update && sudo apt-get install git -y
```
#### *Step#2:*
```
sudo mkdir /home/bootstrap 
```
#### *Step#3:* 
```
cd /home/bootstrap
```

#### *Step#4:* 


```

sudo git clone https://vik_27@bitbucket.org/novigatech/noviga_certificates.git

~~git clone https://lalit_noviga@bitbucket.org/novigatech/noviga_certificates.git~~
```

#### *Step#5:* 
```
cd noviga_certificates
```

#### *Step#6:* 
```
sudo chmod +x *.sh
```

#### *Step#7:* 
```
sudo ./update_user.sh
```

#### *Step#8:* 
```
sudo ./update_pi.sh
```

#### Or this single command
```
sudo mkdir /home/bootstrap && cd /home/bootstrap && sudo git clone https://vik_27@bitbucket.org/novigatech/noviga_certificates.git && cd noviga_certificates && sudo chmod +x *.sh && sudo ./update_user.sh && sudo ./update_pi.sh
```


### *After execution of above script completes, PI will reboot automatically*

### Once Pi is online, follow along


#### *Step#1: Login with ssh certificate using following commands*

##### Locate file *universal* and go to the directory using cd command.
	
```
chmod 600 <location of universal>
ssh -i <location of universal> nucleus@<IP Address> 
```

##### Note: if universal file is in /home/pi then the command for login will be (also ensure permission on universal file is 600):
```
chmod 600 /home/pi/universal
ssh -i /home/pi/universal nucleus@<IP Address>
```

#### *Step#2: Use following commands to clone git repo and run the script:*
```
git clone git@bitbucket.org:novigatech/arduino_cli_download.git /home/nucleus/arduino_cli_download && git clone git@bitbucket.org:novigatech/automation_scripts.git && cd automation_scripts && chmod +x *.sh && source ./pkg_update.sh
```

#### *Step#3: Run commands and scripts in following order:*

```
sudo ./db_update.sh
```

#### *Note:* 

If following error shows up

```
ERROR 1045 (28000): Access denied for user 'root'@'localhost' (using password: NO)
ERROR 1007 (HY000) at line 1: Can't create database 'pidb2'; database exists
Process complete
```

Then going to mysql using following command:

```
mysql-u root -p
```

If you are able to login successfully, then proceed further. **If not able to login to mysql, contact us...** 


#### Next Steps:
Upto this point, `nucleus` is configuration is complete. Before moving further, make sure that you have hostname from the database. Once hostname is obtained, follow the instructions on the page (link below)

[Next Steps Document](https://bitbucket.org/novigatech/automation_scripts/src/master/README.md)

https://bitbucket.org/novigatech/automation_scripts/src/master/README.md

This is lnk for the README file of `automation_scripts` repo.