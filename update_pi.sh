sudo sed -i '/PasswordAuthentication/d' /etc/ssh/sshd_config && sudo echo -e "PasswordAuthentication no" | tee -a /etc/ssh/sshd_config > '/dev/null'
sudo sed -i '/ChallengeResponseAuthentication/d' /etc/ssh/sshd_config && sudo echo -e "ChallengeResponseAuthentication no" | tee -a /etc/ssh/sshd_config > '/dev/null'
sudo sed -i '/UsePAM/d' /etc/ssh/ssh_config && sudo echo -e "UsePAM no" | tee -a /etc/ssh/sshd_config > '/dev/null'

# Initial update of machine
sudo apt-get update

# Installing i2c-tools for hardware clock setup
sudo apt-get install -y i2c-tools

# Updating boot config for disabling bluetooth
# and hwclock setup
                                                                                
sudo sed -i '/dtoverlay=disable-bt/d' /boot/config.txt && sudo echo "dtoverlay=disable-bt" | tee -a /boot/config.txt > '/dev/null'
sudo sed -i '/dtoverlay=i2c-rtc,ds3231/d' /boot/config.txt && sudo echo "dtoverlay=i2c-rtc,ds3231" | tee -a /boot/config.txt > '/dev/null'

sl="$(grep -n 'run\/systemd\/system' /lib/udev/hwclock-set | cut -f1 -d:)"
el=$((sl+2))
sudo sed -i "$sl,$el s/^/#/" /lib/udev/hwclock-set

# Steup local timezone
sudo timedatectl set-timezone Asia/Kolkata
sudo reboot