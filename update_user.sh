sudo adduser nucleus --gecos "First Last,RoomNumber,WorkPhone,HomePhone" --disabled-password
echo "nucleus:RandomN0vig@PaSS" | sudo chpasswd
sudo adduser nucleus sudo
echo 'nucleus ALL=(ALL) NOPASSWD: ALL' | sudo tee /etc/sudoers.d/010_pat-nopasswd
sudo usermod pi -s /sbin/nologin
sudo mkdir -p /home/nucleus/.ssh
sudo chown -R nucleus /home/bootstrap/
sudo chgrp -R nucleus /home/bootstrap/
sudo cat /home/bootstrap/noviga_certificates/universal.pub | sudo tee -a /home/nucleus/.ssh/authorized_keys > '/dev/null'
sudo chmod 600 /home/nucleus/.ssh/authorized_keys
sudo chmod 700 /home/nucleus/.ssh
sudo cp /home/bootstrap/noviga_certificates/universal /home/nucleus/.ssh/
sudo chmod 400 /home/nucleus/.ssh/universal
sudo chown -R nucleus /home/nucleus/.ssh
sudo chgrp -R nucleus /home/nucleus/.ssh
sudo touch /home/nucleus/.bashrc
sudo sed -i '/eval "$(ssh-agent -s)"/d' /home/nucleus/.bashrc
sudo echo 'eval "$(ssh-agent -s)"' | sudo tee -a /home/nucleus/.bashrc > '/dev/null'
sudo sed -i '/ssh-add \/home\/nucleus\/.ssh\/universal/d' /home/nucleus/.bashrc
sudo echo 'ssh-add /home/nucleus/.ssh/universal' | sudo tee -a /home/nucleus/.bashrc > '/dev/null'
sudo chown nucleus /home/nucleus/.bashrc
sudo chgrp nucleus /home/nucleus/.bashrc